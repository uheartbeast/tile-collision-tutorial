{
    "id": "040c9099-6d9d-4854-b302-f3c40e084564",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "93d5c7f3-5a19-427a-8107-859204af1192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "040c9099-6d9d-4854-b302-f3c40e084564",
            "compositeImage": {
                "id": "6ee77842-a460-4416-82f0-e53411723d4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93d5c7f3-5a19-427a-8107-859204af1192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3782487a-f30a-4f27-99dd-7601b3885a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93d5c7f3-5a19-427a-8107-859204af1192",
                    "LayerId": "d8016889-219d-493b-8c3b-2e3b18099837"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d8016889-219d-493b-8c3b-2e3b18099837",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "040c9099-6d9d-4854-b302-f3c40e084564",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}