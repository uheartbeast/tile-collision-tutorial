{
    "id": "45022562-be9c-401a-95e2-02b7d0b177ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "34f31931-dcad-407c-96a7-2cc138698bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45022562-be9c-401a-95e2-02b7d0b177ec",
            "compositeImage": {
                "id": "7e374856-f520-4776-96aa-657d0a49b72c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34f31931-dcad-407c-96a7-2cc138698bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead653ee-8cd4-4279-a1e1-b37c87bacff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34f31931-dcad-407c-96a7-2cc138698bb7",
                    "LayerId": "c7db8434-a61f-498e-9389-008137802b61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c7db8434-a61f-498e-9389-008137802b61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45022562-be9c-401a-95e2-02b7d0b177ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}