{
    "id": "e825f578-8744-4736-949c-5fc596812adb",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "t_collisions",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 1,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "040c9099-6d9d-4854-b302-f3c40e084564",
    "sprite_no_export": true,
    "textureGroup": 0,
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 2,
    "tileheight": 64,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 64,
    "tilexoff": 0,
    "tileyoff": 0
}